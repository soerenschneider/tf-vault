resource "vault_quota_rate_limit" "auth" {
  name = "auth"
  path = "auth/token/"
  rate = 5
}

resource "vault_token_auth_backend_role" "prometheus" {
  role_name         = vault_policy.prometheus.name
  allowed_policies  = [vault_policy.prometheus.name, "default"]
  token_bound_cidrs = var.prometheus_token_cidrs
  orphan            = true
}

resource "vault_token" "prometheus" {
  depends_on = [vault_token_auth_backend_role.prometheus]
  role_name  = "prometheus"
  policies = [
    vault_policy.prometheus.name
  ]
  display_name = "prometheus-access"
  renewable    = true
  num_uses     = 0
  ttl          = var.prometheus_token_ttl
}

resource "vault_policy" "prometheus" {
  name = "prometheus"

  policy = <<EOT
path "sys/metrics*" {
  capabilities = ["read", "list"]
}
EOT
}

resource "vault_token_auth_backend_role" "emergency" {
  role_name        = vault_policy.emergency.name
  allowed_policies = [vault_policy.emergency.name, "default"]
  orphan           = true
}

resource "vault_token" "emergency" {
  depends_on = [vault_token_auth_backend_role.emergency]
  role_name  = "emergency"
  policies = [
    vault_policy.emergency.name
  ]
  display_name = "emergency-seal"
  renewable    = true
  num_uses     = 1
  ttl          = var.emergency_token_ttl
}

resource "vault_policy" "emergency" {
  name = "emergency"

  policy = <<EOT
path "/sys/seal" {
  capabilities = ["create", "update", "sudo"]
}
EOT
}
