state-delete:
	test -f terraform.tfstate.secret && rm -fv terraform.tfstate
	test -f terraform.tfstate.backup.secret && rm -vf terraform.tfstate.backup

state-encrypt:
	git secret hide
	rm -vf terraform.tfstate.backup
	rm -vf terraform.tfstate

state-decrypt:
	git secret reveal

init:
	terraform init

plan:
	terraform plan

apply:
	terraform apply

apply-force:
	terraform apply -auto-approve

destroy:
	terraform destroy
