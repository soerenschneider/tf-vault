terraform {
  required_providers {
    vault = {
      source  = "vault"
      version = "~> 2.21.0"
    }
  }
}

provider "vault" {
  address               = var.terraform_url
  max_lease_ttl_seconds = 120
}
