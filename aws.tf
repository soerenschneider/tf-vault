locals {
  aws_path    = "aws"
}

resource "vault_aws_secret_backend" "aws" {
  max_lease_ttl_seconds     = 3600
  default_lease_ttl_seconds = 600
}

resource "vault_aws_secret_backend_role" "dyndns" {
  depends_on      = [vault_aws_secret_backend.aws]
  backend         = local.aws_path
  name            = "dyndns"
  credential_type = "iam_user"

  policy_document = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "route53:ChangeResourceRecordSets",
      "Resource": "arn:aws:route53:::hostedzone/${var.aws_hosted_zone}"
    }
  ]
}
EOT
}

resource "vault_aws_secret_backend_role" "acmevault" {
  depends_on      = [vault_aws_secret_backend.aws]
  backend         = local.aws_path
  name            = "acmevault"
  credential_type = "iam_user"

  policy_document = <<EOT
{
   "Version": "2012-10-17",
   "Statement": [
       {
           "Sid": "",
           "Effect": "Allow",
           "Action": [
               "route53:GetChange",
               "route53:ChangeResourceRecordSets",
               "route53:ListResourceRecordSets"
           ],
           "Resource": [
               "arn:aws:route53:::hostedzone/*",
               "arn:aws:route53:::change/*"
           ]
       },
       {
           "Sid": "",
           "Effect": "Allow",
           "Action": "route53:ListHostedZonesByName",
           "Resource": "*"
       }
   ]
}
EOT
}

resource "vault_policy" "acmevault" {
  name = "acmevault"

  policy = <<EOT
path "/aws/creds/${vault_aws_secret_backend_role.acmevault.name}" {
  capabilities = ["read"]
}
EOT
}

resource "vault_policy" "dyndns" {
  name = "dyndns"

  policy = <<EOT
path "/aws/creds/${vault_aws_secret_backend_role.dyndns.name}" {
  capabilities = ["read"]
}
EOT
}

resource "vault_token_auth_backend_role" "dyndns" {
  role_name         = "dyndns"
  allowed_policies  = [vault_policy.dyndns.name, "default"]
  token_bound_cidrs = var.dyndns_server_cidrs
  orphan            = true
}

resource "vault_token" "dyndns" {
  depends_on = [vault_token_auth_backend_role.dyndns]
  role_name  = "dyndns"
  policies = [
    vault_policy.dyndns.name
  ]
  display_name = "dyndns-access"
  renewable    = true
  num_uses     = 0
  ttl          = 3600
}
