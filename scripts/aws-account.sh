#!/bin/sh


aws iam create-policy --policy-name vault --policy-document file://role-vault.json

aws iam create-user --user-name vault-ez

aws iam attach-user-policy --user-name vault-ez --policy-arn 'arn:aws:iam:aws:policy/vault'

aws iam create-access-key --user-name vault-ez
