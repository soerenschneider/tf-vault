#!/bin/sh

PW=${1:-supersecurepw}

vault server -dev -dev-root-token-id="${PW}"

export VAULT_ADDR=http://localhost:8200
export VAULT_TOKEN="${PW}"
