#!/bin/sh

PW=${1:-supersecurepw}

curl --header "X-Vault-Token: ${PW}" -X DELETE http://127.0.0.1:8200/v1/sys/mounts/secret
