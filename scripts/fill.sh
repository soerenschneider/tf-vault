#!/bin/sh

VAULT_TOKEN_FILE=~/.vault-token

#trap clear_token ERR EXIT

load_token() {
    if [ -n "${VAULT_TOKEN}" ]; then
        return
    fi

    if [ -f "${VAULT_TOKEN_FILE}" ]; then
        VAULT_TOKEN=$(cat "${VAULT_TOKEN_FILE}")
    else
        echo "Could not determine vault token"
        exit 1
    fi
}

insert_simple_secret() {
    if [ -z "$1" ]; then
        echo "Can not insert simple secret, no secret name provided"
    fi
    if [ -z "$2" ]; then
        echo "Can not insert simple secret, no secret payload provided"
    fi

    curl \
    -H "X-Vault-Token: ${VAULT_TOKEN}" \
    -H "Content-Type: application/json" \
    -X POST \
    -d "{\"value\":\"$2\"}" \
    "${VAULT_ADDR}/v1/secret/$1"
}

init_aws() {
    local access_key=$(cat ~/.aws/credentials | grep ^aws_access_key_id | awk '{print $3}')
    local secret_key=$(cat ~/.aws/credentials | grep ^aws_secret_access_key | awk '{print $3}')
    local region="us-east-1"

    curl \
    --header "X-Vault-Token: ${VAULT_TOKEN}" \
    --request POST \
    --data "{\"access_key\": \"${access_key}\",\"secret_key\": \"${secret_key}\",\"region\": \"${region}\"}" \
    "${VAULT_ADDR}/v1/aws/config/root"
}

clear_token() {
    echo Clearing vault token
    unset VAUL_TOKEN
}

load_token
init_aws
insert_simple_secret test 8973245823432
