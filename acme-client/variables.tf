variable policies {
  type = list(string)

  validation {
    condition     = length(var.policies) > 0 
    error_message = "At least one policy name must be supplied."
  }
}

variable approle_path {
  type = string
}

variable role_id {
  type = string
}

variable secret_id_cidrs {
  type = list(string)
}

variable secret_id_ttl {
  type = number
  default = 7776000

  validation {
    condition     = var.secret_id_ttl >= 3600 && var.secret_id_ttl <= 7776000
    error_message = "Supply values between 3600 (1 hour) and 7776000 (3 months) only."
  }
}
