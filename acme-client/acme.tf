resource "vault_approle_auth_backend_role" "acme_client" {
  backend                = var.approle_path
  role_name              = "acme-client-${var.role_id}"
  token_policies         = var.policies
  role_id                = var.role_id
  secret_id_bound_cidrs  = var.secret_id_cidrs
  secret_id_ttl          = var.secret_id_ttl
  token_explicit_max_ttl = 180
  token_bound_cidrs      = var.secret_id_cidrs
}

resource "vault_approle_auth_backend_role_secret_id" "acme_client" {
  backend   = var.approle_path
  role_name = vault_approle_auth_backend_role.acme_client.role_name
  metadata  = "{\"cert\": \"${var.role_id}\"}"
}

resource "vault_approle_auth_backend_login" "acme_client" {
  backend   = var.approle_path
  role_id   = vault_approle_auth_backend_role.acme_client.role_id
  secret_id = vault_approle_auth_backend_role_secret_id.acme_client.secret_id
}
