
variable terraform_url {
  default = "https://vault.dd.soerenschneider.net:8200"
}

variable aws_hosted_zone {
  type = string
  default = "Z09025662ISFIZ6MH3O8I"
}

variable gocryptfs_media_token_ttl {
  type = string
  default = "9600h"
}

variable "acme_server_cidrs" {
  type = list(string)
  default = [
    "192.168.64.0/24",
    "192.168.65.0/24",
    "192.168.200.0/24"
  ]
}

variable "dyndns_server_cidrs" {
  type = list(string)
  default = [
    "192.168.65.0/24",
  ]
}

variable "acme_server_secret_id_ttl" {
  type    = number
  default = 7776000
}

variable "prometheus_token_cidrs" {
  type    = list(string)
  default = []
}

variable "prometheus_token_ttl" {
  type    = string
  default = "9600h"
}

variable "emergency_token_ttl" {
  type    = string
  default = "9600h"
}

variable "audit_file_path" {
  type    = string
  #default = "/etc/vault.d/audit.log"
  default = "/tmp/audit.log"
}

variable "acme_clients" {
  type = map(any)
  default = {
    hass = {
      role_id = "hass.dd.soerenschneider.net"
      secret_id_cidrs = [
        "192.168.64.0/24",
        "192.168.65.0/24",
        "192.168.200.0/24"
      ]
    },
    sauron = {
      role_id = "sauron.dd.soerenschneider.net"
      secret_id_cidrs = [
        "192.168.64.0/24",
        "192.168.65.0/24",
        "192.168.200.0/24"
      ]
    }
  }
}
