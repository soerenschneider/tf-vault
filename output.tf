output "prometheus-token" {
  value = vault_token.prometheus.client_token
  sensitive = true
}

output "dyndns-token" {
  value = vault_token.dyndns.client_token
  sensitive = true
}

output "emergency-token" {
  value = vault_token.emergency.client_token
  sensitive = true
}

output "media-unlocker" {
  value = vault_token.gocryptfs_media.client_token
  sensitive = true
}

output "acme-server-secret-id" {
  value = vault_approle_auth_backend_login.acme_server.secret_id
  sensitive = true
}

output "acme-server-role-id" {
  value = vault_approle_auth_backend_login.acme_server.role_id
}

output "acme-client-secret-id" {
  value = module.acme-client
  sensitive = true
}

#output "acme-client-role-id" {
#    value = vault_approle_auth_backend_login.acme_client.role_id
#}
