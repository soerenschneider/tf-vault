resource "vault_quota_rate_limit" "secret" {
  depends_on = [
    vault_mount.kv
  ]
  name       = "secret"
  path       = "secret/"
  rate       = 5
}

resource "vault_mount" "kv" {
  path        = "secret"
  type        = "kv"
  description = "Secret kv mount"
}

resource "vault_policy" "gocryptfs_media" {
  name = "gocryptfs_media"

  policy = <<EOT
path "secret/data/gocryptfs/media" {
  capabilities = ["read"]
}
EOT
}

resource "vault_token_auth_backend_role" "gocryptfs_media" {
  role_name        = vault_policy.gocryptfs_media.name
  allowed_policies = [
    vault_policy.gocryptfs_media.name,
    "default"
  ]
  orphan           = true
  token_bound_cidrs = [
    "127.0.0.1",
    "192.168.2.216",
    "192.168.65.12"
  ]
}

resource "vault_token" "gocryptfs_media" {
  depends_on = [vault_token_auth_backend_role.gocryptfs_media]
  role_name  = "gocryptfs_media"
  policies = [
    vault_policy.gocryptfs_media.name
  ]
  display_name = "gocryptfs-media"
  renewable    = true
  ttl          = var.gocryptfs_media_token_ttl
}
