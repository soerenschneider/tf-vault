resource "vault_auth_backend" "user" {
  type = "userpass"

  tune {
    max_lease_ttl = "3600s"
  }
}
