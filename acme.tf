resource "vault_auth_backend" "approle" {
  type = "approle"
}

resource "vault_policy" "acme_server" {
  name = "acme-server"

  policy = <<EOT
path "secret/acme/*" {
  capabilities = ["list", "create", "update", "read", "delete"]
}
EOT
}

resource "vault_approle_auth_backend_role" "acme_server" {
  backend                = vault_auth_backend.approle.path
  role_name              = "acme-server"
  token_policies         = [vault_policy.acme_server.name]
  token_explicit_max_ttl = 86500
  secret_id_bound_cidrs  = var.acme_server_cidrs
  secret_id_ttl          = var.acme_server_secret_id_ttl
  role_id                = "acme-server"
}

resource "vault_approle_auth_backend_role_secret_id" "acme_server" {
  backend   = vault_auth_backend.approle.path
  role_name = vault_approle_auth_backend_role.acme_server.role_name
}

resource "vault_approle_auth_backend_login" "acme_server" {
  backend   = vault_auth_backend.approle.path
  role_id   = vault_approle_auth_backend_role.acme_server.role_id
  secret_id = vault_approle_auth_backend_role_secret_id.acme_server.secret_id
}

resource "vault_policy" "acme_client" {
  name = "acme-client"

  policy = <<EOT
path "/secret/acme/client/{{identity.entity.aliases.${vault_auth_backend.approle.accessor}.metadata.cert}}" {
  capabilities = ["read"]
}
EOT
}

module "acme-client" {
  for_each        = var.acme_clients
  source          = "./acme-client"
  approle_path    = vault_auth_backend.approle.path
  role_id         = each.value.role_id
  secret_id_ttl   = 7776000
  secret_id_cidrs = each.value.secret_id_cidrs
  policies        = [vault_policy.acme_client.name]
}
