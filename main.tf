resource "vault_quota_rate_limit" "global" {
  name = "global"
  path = ""
  rate = 50
}

resource "vault_audit" "file" {
  type = "file"

  options = {
    file_path = var.audit_file_path
  }
}
